<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\SignupRequest;
use Illuminate\Http\Request;

class UserController extends Controller
{
	public function login(LoginRequest $request)
	{
		// Check if request has correct credentials.
		if(!Auth::attempt($request->only('email', 'password')))
		{
			return request()->json(['status'=>'Unauthorised', 'message'=>'Incorrect User\'s email or Password'], 401);
		}
		
		// If credentials are correct; then generate tokens;
		$user = Auth::user();
		$tokenResult = $user->createToken('Token');
		$token = $tokenResult->token;
		$token->save();

		// If user wants token to last longer;
		if($request->remember_me)
		{
			$token->expires_at = Carbon::now()->addWeeks(1);
		}

		return response()->json([
			'status'=>'Successful',
			'message'=>'You are now logged in!',
			'as' => $user,
			'token'=>$tokenResult->accessToken,
			'token_type'=>'Bearer',
			'expires_at'=>$token->expires_at,
		]);
	}

	public function signup(SignupRequest $request){
		$user = new User([
			'name'=> $request->name,
			'email'=> $request->email,
			'password'=> bcrypt($request->password)
		]);

		$user->save();

		return response()->json(['status'=>'Successful!', 'user'=>$user],201);
	}
	
	public function logout(Request $request){
		$user = $request->user();
		$user->token()->revoke();

		return response()->json(['status'=>'Successful','message'=>'You are logged out.']);
	}


	public function user(Request $request){
		return response()->json($request->user());
	}
	
}

