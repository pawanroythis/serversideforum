<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Routes Specific for user */

	/* When User is not authenticated */
Route::group(['prefix'=>'auth'], function(){
	Route::post('signup', 'UserController@signup');
	Route::post('login', 'UserController@login');
});
	/* When User is authenticated */
Route::group(['middleware'=>'auth:api'], function(){
	Route::get('user', 'UserController@user');
	Route::get('logout', 'UserController@logout');
});


